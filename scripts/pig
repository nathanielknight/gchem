#!python
'''
Copyright (C) 2012 Nat Egan-Pimblett

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Contact
'''

import sys, argparse, itertools
import gchem


VERSION = 0.3

#Argument Parser------------------------------------------------------
p = argparse.ArgumentParser(
        description="Command line interface to GEOS-Chem files.",
        prog='pig.py')
#--general options
p.add_argument('-v', action="version", version="%(prog)s {0}".format(VERSION))
p.add_argument('command', help="Action to perform on the input file.",
        type=str, choices=['ls', 'cat'])
p.add_argument('infile', help='Input File.', type=argparse.FileType('r'))
p.add_argument('-o', '--outfile', help="Output file.", default=sys.stdout, 
        metavar='OUTPUT_FILE', type=argparse.FileType('w'))
p.add_argument("--headless", action="store_true", 
        help="Suppress table headers.")
#--ls options
l_opts = p.add_argument_group("ls options", "Printing meta-data.")
#--cat options
c_opts = p.add_argument_group("cat options", "Printing contents.")
c_opts.add_argument("-s", "--slice", help="Slice of datablock to print.")
c_opts.add_argument("datablock", help="Name of datablock to print",
        default=None, nargs="?")



#ls implementation----------------------------------------------------
def bpch_metadata(bpch_file, headless=False):
    if not headless:
        head = "\nBPCH File: {0}\nContains: \"{1}\"\n".format(
                bpch_file.name, bpch_file.title)
    else:
        head = ""
    blocks = map(
            lambda i, b: "{0}  {1: <8}({2})  {3}".format(i,b.name, b.unit, b.shape),
            *zip(*enumerate(bpch_file.datablocks)))
    return head + '\n'.join(blocks) + '\n'


#cat implementation---------------------------------------------------


_xgrid = gchem.grids.c_lon_4x5
_ygrid = gchem.grids.c_lat_4x5

def bpch_content(bpch_file, headless=False, datablock=None):
    #check args
    if not headless:
        head = "\nContents of Datablock {0} from file {1}\n".format(
                datablock, bpch_file.name)
    else:
        head = ''
    if not datablock:
        raise argparse.ArgumentError(None, "Need a datablock to print.")
    if datablock.isdigit():
        datablock = int(datablock)
    if type(datablock) is int:
        db = bpch_file.filter(number=datablock)[0]
    elif type(datablock) is string:
        db = bpch_file.filter(name=datablock)[0]
    else:
        raise TypeError("Expected datablock to be name or number")
    #print values

    def value(i,j):
        return "{} {} {}".format(_xgrid[i], _ygrid[j], db.value[i,j,0])

    data = "\n\n".join(
            ["\n".join( 
                [value(i,j) for j in range(db.shape[1])]) 
                for i in range(db.shape[0])])
    return head + data + "\n"


#Execution
if __name__ == "__main__":
    args = p.parse_args()
    infile = gchem.bpch.File.fromfile(args.infile.name)
    if args.command == 'ls':
        args.outfile.write(bpch_metadata(
            infile, 
            headless=args.headless))
    elif args.command == 'cat':
        args.outfile.write(bpch_content(
            infile, 
            datablock=args.datablock,
            headless=args.headless))
